#Assembly tools

## Band tool

The band tool needs to be bent before use. It is the longer of the two tools.

## Nut tool

The nut tool is optional, but useful. It is the shorter of the two tools.

[Download STL](models/actuator_assembly_tools.stl)