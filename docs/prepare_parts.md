# Prepare the parts

## Remove brim {pagestep}

If you used any brim during printing, remove it with your fingers, or pliers. Deburring tools or other sharp tools can be used to trim off the remaining brim, but care must be taken to avoid injury. A small amount of residual brim will not adversely affect the block stage's operation.

## Cut the ties {pagestep}

On the [main body](fromstep){Qty: 1, cat:3DPrinted} there are small plastic ties which help the actuator columns stay aligned when printing. Use [precision wire cutters]{Qty:1} to cut these ties. There are two ties to cut inside each column.  
![](images/1-1-Ties.jpg)
![](images/1-2-Cut.jpg)
![](images/1-3-NoTies.jpg)

[precision wire cutters]: "{cat:Tool}"

## Tapping the holes at the sides of the the stages {pagestep}

The holes on the fixed stage are somewhat triangular. This is to allow a normal machine screw to easily make a thread. 

>! Do not use a normal M3 tap, it will sit too low in the hole and damage the plastic.

Tap a hole (make the tread) on the moving stage using a normal [M3x6mm screw] by screwing a screw into it. Make sure to hold the screw vertical and then drive it into the plastic until it reaches the bottom. **Do not over torque the screw or you will damage the plastic.** You can now remove the screw.

>! The holes do not go all the way through the stage, as they would not print properly otherwise. This means you must be very careful not to "bottom out" the screw: if you screw it in more than 3-4mm, you are likely to damage the part.

Repeat for all eight holes on the fixed stage.  
![](images/5-1-Tap.jpg)![](images/5-2-Tap.jpg)

[prepared main body]{output, qty:1, hidden}

Repeat this process to tap the 8 holes at the edge of the [moving platform](fromstep){Qty: 1, cat:3DPrinted}.

[prepared moving platform]{output, qty:1, hidden}
