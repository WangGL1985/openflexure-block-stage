# OpenFlexure Block Stage
A 3D Printable high-precision 3 axis translation stage.

This project is a 3D printable design that enables very fine (sub-micron) mechanical positioning of a small moving stage, with surprisingly good mechanical stability.  It follows on from the [OpenFlexure Microscope](https://openflexure.org/projects/microscope/).  Currently, it's designed to function as a more-or-less drop in replacement for fibre alignment stages available from various scientific suppliers, but the project aims to be useful to electrophysiologists, nanotechnology folk, and many more.  We have recently described it in a [pre-print on arXiv](https://arxiv.org/abs/1911.09986)

![A render ofan OpenFlexure Block Stage](docs/images/blockstage_render.png)

## Kits and License
This project is open-source and is released under the CERN open hardware license.  We are working on bring able to sell kits through [OpenFlexure Industries Ltd.](https://www.openflexure.com/), and will update here once we have a good way of doing it.



## Printing/building it yourself
[**Full build instructions are available here**](https://openflexure.gitlab.io/openflexure-block-stage/), inclduing downloads of `.stl` files for printing. You can get all the `.stl` files as a `.zip` from the relevant page of the instruction. Older versions may be available from the [build server](https://build.openflexure.org/openflexure-block-stage/).

## Get Involved

This project is open so that anyone can get involved, and you don't have to learn OpenSCAD to help (although that would be great).  Ways you can contribute include:

* Raise [issues](https://gitlab.com/openflexure/openflexure-block-stage/issues) if there are things that aren't clear.
* Help us write the instructions. We are planning on writing our instructions in markdown (with some [extra meta-data to automate the bill of materials](https://gitlab.com/bath_open_instrumentation_group/git-building)).
* Improvements to the code, or even just sharing parameters you used (if you customised it) and how well it worked would be great. 
* Share your images of your OpenFlexure Block Stage on social media - you can mention @openflexure on Twitter

## Developing
If you want to play with the OpenSCAD files or change the documentation, you should fork the repository.  You can edit the documentation online in GitLab, or clone the repository if you want to edit the OpenSCAD files.  NB you'll need to clone the whole repository as the OpenSCAD files are dependent on each other.


### LFS files

This repository will store images using Git LFS, and is set up not to download these to your computer.  This is intended to make life easier for the members of our community who don't have fast internet connections.  If you want to download these, you can enable it with:
```bash
git lfs install
git config --local lfs.fetchexclude ""
git lfs fetch
git lfs checkout
```
This will download all the images. This may take a while.
